﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using VA_ExternalServiceBase;

namespace HR_EmpHistMigration
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            this.LoadSettings();
        }

        private void rbSourceOracle_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSourceOracle.Checked)
            {
                txtSourceServer.Enabled = false;
                txtSourceServer.Text = "";
                rbSourceSQL.Checked = false;
                txtSourceDatabase.Focus();

            }
        }

        private void rbSourceSQL_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSourceSQL.Checked)
            {
                txtSourceServer.Enabled = true;
                rbSourceOracle.Checked = false;
                txtSourceServer.Focus();
            }
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int count = 0;

                // Validate
                if (this.ValidateFields())
                {
                    // Get settings
                    string connectionString = this.GetConnectionString();
                    this.SaveSettings();

                    // Do updates
                    EmpHistMigration mngr = new EmpHistMigration();
                        try
                        {
                        count = mngr.UpdatePersons(connectionString);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString(), "Exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        // Display results
                        MessageBox.Show(string.Format("{0} anställda har fått ändrat ikraftträdande datum",count), "Message");
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadSettings()
        {
            if (Properties.Settings.Default.SourceDBType == "ORACLE")
                rbSourceOracle.Checked = true;
            else
                rbSourceSQL.Checked = true;
            txtSourceServer.Text = Properties.Settings.Default.SourceServer;
            txtSourceDatabase.Text = Properties.Settings.Default.SourceDatabase;
            txtSourceUser.Text = Properties.Settings.Default.SourceUser;
            txtSourcePassword.Text = Properties.Settings.Default.SourcePassword;
        }

        private void SaveSettings()
        {
            if (rbSourceOracle.Checked)
                Properties.Settings.Default.SourceDBType = "ORACLE";
            else
                Properties.Settings.Default.SourceDBType = "SQL Server";
            Properties.Settings.Default.SourceServer = txtSourceServer.Text;
            Properties.Settings.Default.SourceDatabase = txtSourceDatabase.Text;
            Properties.Settings.Default.SourceUser = txtSourceUser.Text;
            Properties.Settings.Default.SourcePassword = txtSourcePassword.Text;

            Properties.Settings.Default.Save();
        }


        private bool ValidateFields()
        {
            if (rbSourceSQL.Checked && txtSourceServer.Text.Trim().Length == 0)
            {
                MessageBox.Show("A server must be given", "Exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSourceServer.Focus();
                return false;
            }
            if (txtSourceDatabase.Text.Trim().Length == 0)
            {
                MessageBox.Show("A database must be given", "Exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSourceDatabase.Focus();
                return false;
            }
            if (txtSourceUser.Text.Trim().Length == 0)
            {
                MessageBox.Show("A user must be given", "Exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSourceUser.Focus();
                return false;
            }
            if (txtSourcePassword.Text.Trim().Length == 0)
            {
                MessageBox.Show("A password must be given", "Exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSourcePassword.Focus();
                return false;
            }

            return true;
        }

        private string GetConnectionString()
        {
            string connectionString;
            if (rbSourceOracle.Checked)
            {
                connectionString =
                    string.Format("Provider=OraOLEDB.Oracle; Data Source={0}; User ID={1};Password={2}",
                                    txtSourceDatabase.Text,
                                    txtSourceUser.Text,
                                    txtSourcePassword.Text);
            }
            else
            {
                connectionString =
                    string.Format("Provider=SQLOLEDB; Data Source={0}; Database={1};User ID={2};Password={3}",
                                    txtSourceServer.Text,
                                    txtSourceDatabase.Text,
                                    txtSourceUser.Text,
                                    txtSourcePassword.Text);
            }
            return connectionString;
        }
       
    }
}
