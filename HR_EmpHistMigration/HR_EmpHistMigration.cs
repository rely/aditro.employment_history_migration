﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using VA_ExternalServiceBase;
using Persona.Common;
using System.Configuration;
using System.Net.Mail;

namespace HR_EmpHistMigration
{
    public enum UserType
    {
        All,
        SSO,
        Selected,
        Single
    }

    public class EmpHistMigration
    {
        protected string _connectionString;
        protected SQLHelper _sqlReader;
        protected Random _random = new Random();
        protected StreamWriter _writer = null;
        protected DataSet _dsUserProperties = null;

        protected const string CHANGING_USER = "PASYS";
        protected string _timestamp = DateTime.Now.ToString("yyyyMMdd HHmmss");

        public int UpdatePersons(string connectionString)
        {
            int count = 0;
            try
            {
                string logFile = ConfigurationManager.AppSettings["LogFile"];
                if (!string.IsNullOrEmpty(logFile))
                    _writer = File.CreateText(logFile);

                if (!string.IsNullOrEmpty(connectionString))
                {
                    _connectionString = connectionString;
                    _sqlReader = new SQLHelper(_connectionString);

                    count = this.UpdateHistory();
                }
            }
            finally
            {
                if (_writer != null)
                {
                    _writer.Close();
                    _writer.Dispose();
                    _writer = null;
                }
            }
            return count;
        }


        protected int UpdateHistory()
        {
            string CompanyId = "";
            string EmpNo = "";
            string Commdate;
            string Changedate;
            DateTime EmpdateDate;
            string Empdate;
            int count = 0;
            StringBuilder sql = new StringBuilder();
            sql.Append("select p.foretag, p.anstnr, pb.IKRAFTT_DATUM, pb.andr_datum"
                        + ", (select top 1 TILL from p_historik ph where p.foretag = ph.foretag and p.anstnr = ph.anstnr and ph.kolumn_namn = 'ANSTDATUM' order by TILL asc)  as 'anstdatum'"
                        + " from person p"
                        + " inner join p_historik pb on p.foretag = pb.foretag and p.anstnr = pb.anstnr and pb.KOLUMN_NAMN = 'ANSTFORHALL'"
                        + " where Cast((select top 1 TILL from p_historik ph where p.foretag = ph.foretag and p.anstnr = ph.anstnr and ph.kolumn_namn = 'ANSTDATUM' order by TILL asc) as datetime) < Cast(pb.IKRAFTT_DATUM as datetime) order by p.foretag, p.anstnr, pb.ikraftt_datum");

                OleDbDataReader oleReader = (OleDbDataReader)_sqlReader.ExecuteReader(sql.ToString());
                if (oleReader.HasRows)
                {
                    while (oleReader.Read())
                    {
                        if (CompanyId != oleReader["FORETAG"].ToString() || EmpNo != oleReader["ANSTNR"].ToString())
                        { 
                            CompanyId = oleReader["FORETAG"].ToString();
                            EmpNo = oleReader["ANSTNR"].ToString();
                            Commdate = oleReader["IKRAFTT_DATUM"].ToString();
                            EmpdateDate = Convert.ToDateTime(oleReader["ANSTDATUM"].ToString());
                            Empdate = EmpdateDate.ToString("yyyyMMdd");
                            Changedate = oleReader["ANDR_DATUM"].ToString();
                        UpdateHistoryInfo(CompanyId, EmpNo, Commdate, Empdate, Changedate);
                            count += 1;
                        }
                    }
                }
                return count;
        }
                
        protected int UpdateHistoryInfo(string company, string empno, string commdate, string empdate, string changedate)
        {
            int result = 0;
            StringBuilder sql;
            OleDbTransaction transaction = null;
            SQLHelper sqlChanger = null;
            try
            {
                sqlChanger = new SQLHelper(_connectionString);
                transaction = sqlChanger.GetTransaction();

                sql = new StringBuilder();
                sql.AppendFormat("update p_historik set ikraftt_datum = '{0}', andr_datum = '{1}', anvandare = '{2}'"
                    + " where foretag = '{3}' and anstnr = '{4}' and KOLUMN_NAMN = 'ANSTFORHALL' and andr_datum = '{5}' and IKRAFTT_DATUM = '{6}' ", empdate, this._timestamp, CHANGING_USER, company, empno, changedate, commdate);
                Debug.WriteLine(sql.ToString());

                result = sqlChanger.ExecuteChange(sql.ToString(), transaction);
                if (result == 1)
                {
                    transaction.Commit();
                    transaction = null;
                    _writer.WriteLine("Ändrat ikraftträdande datum från {0} till {1} för anställd {2}/{3}", commdate, empdate, company, empno);
                }
                else
                {
                    _writer.WriteLine("Fel. Kan inte ändra datum {0} till {1} för anställd {2}/{3}. Får fler träffar.", commdate, empdate, company, empno);
                    transaction.Rollback();
                    transaction = null;
                }
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                    transaction = null;
                    result = 0;
                }
                if (sqlChanger != null)
                    sqlChanger.Dispose();
                sqlChanger = null;
            }
            return result;
        }


        public void Log(string logMessage)
        {
            _writer.Write("Log Entry: ");
            _writer.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
            _writer.WriteLine("\t{0}", logMessage);
            _writer.WriteLine("-----------------------------------------------------------------------------\n");
        }
    }
}
