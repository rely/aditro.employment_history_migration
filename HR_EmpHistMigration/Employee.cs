﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace HR_EmpHistMigration
{
    public class Employment
    {
        public readonly long EmploymentNo;
        public long? CompanyId;
        public long? EmpNo;

        public Employment(long employmentNo)
        {
            this.EmploymentNo = employmentNo;
        }
    }
}
