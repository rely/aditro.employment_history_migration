﻿Imports System.Data.OleDb
Imports System.Collections.Generic

Public Class SQLHelper
    Implements IDisposable

    Private m_objConnection As OleDbConnection

    Public Sub New(ByVal p_strConnection As String)
        m_objConnection = New OleDbConnection(p_strConnection)
        m_objConnection.Open()
    End Sub

    ' Executes Select statements and returns a DataSet
    Public Function ExecuteSelect(ByVal selectCommand As String, Optional ByVal transaction As OleDbTransaction = Nothing, Optional params As List(Of OleDbParameter) = Nothing, Optional commandTimeout As Integer = 0) As DataSet
        Dim blnLocalTransaction As Boolean = False
        If transaction Is Nothing Then
            transaction = m_objConnection.BeginTransaction()
            blnLocalTransaction = True
        End If

        Try
            Dim command As OleDbCommand = New OleDbCommand(selectCommand, m_objConnection, transaction)
            If params IsNot Nothing Then
                For Each param As OleDbParameter In params
                    command.Parameters.Add(param)
                Next
            End If
            If commandTimeout > 0 Then command.CommandTimeout = commandTimeout
            Dim adapter As New OleDbDataAdapter(command)
            Dim data As New DataSet()
            adapter.Fill(data)
            adapter.Dispose()
            adapter = Nothing
            command.Dispose()
            command = Nothing

            If blnLocalTransaction Then transaction.Commit()

            Return data
        Catch ex As Exception
            If blnLocalTransaction Then transaction.Rollback()
            Throw
        End Try
    End Function

    ' Executes Select statements and returns a DataReader
    Public Function ExecuteReader(ByVal selectCommand As String, Optional ByVal transaction As OleDbTransaction = Nothing, Optional params As List(Of OleDbParameter) = Nothing, Optional commandTimeout As Integer = 0) As IDataReader
        Try
            Dim command As OleDbCommand = New OleDbCommand(selectCommand, m_objConnection, transaction)
            If params IsNot Nothing Then
                For Each param As OleDbParameter In params
                    command.Parameters.Add(param)
                Next
            End If
            If commandTimeout > 0 Then command.CommandTimeout = commandTimeout
            Dim reader As OleDbDataReader = command.ExecuteReader()

            command.Dispose()
            command = Nothing

            Return reader
        Catch ex As Exception
            Throw
        End Try
    End Function

    ' Executes Select statements where only first row, first column should be returned
    Public Function ExecuteScalar(ByVal selectCommand As String, Optional ByVal transaction As OleDbTransaction = Nothing, Optional params As List(Of OleDbParameter) = Nothing, Optional commandTimeout As Integer = 0) As Object
        Dim blnLocalTransaction As Boolean = False
        If transaction Is Nothing Then
            transaction = m_objConnection.BeginTransaction()
            blnLocalTransaction = True
        End If

        Try
            Dim command As OleDbCommand = New OleDbCommand(selectCommand, m_objConnection, transaction)
            If params IsNot Nothing Then
                For Each param As OleDbParameter In params
                    command.Parameters.Add(param)
                Next
            End If
            If commandTimeout > 0 Then command.CommandTimeout = commandTimeout
            Dim result As Object = command.ExecuteScalar()
            command.Dispose()
            command = Nothing

            If blnLocalTransaction Then transaction.Commit()

            Return result
        Catch ex As Exception
            If blnLocalTransaction Then transaction.Rollback()
            Throw
        End Try
    End Function

    ' Executes Select Count(*) statements
    Public Function ExecuteCount(ByVal selectCommand As String, Optional ByVal transaction As OleDbTransaction = Nothing, Optional params As List(Of OleDbParameter) = Nothing, Optional commandTimeout As Integer = 0) As Integer
        Dim result As Object = Me.ExecuteScalar(selectCommand, transaction, params, commandTimeout)
        If result Is Nothing Then
            Return 0
        ElseIf IsDBNull(result) Then
            Return 0
        Else
            Return CInt(result)
        End If
    End Function

    ' Executes Insert/Update/Delete statements
    Public Function ExecuteChange(ByVal sqlCommand As String, Optional ByVal transaction As OleDbTransaction = Nothing, Optional params As List(Of OleDbParameter) = Nothing, Optional commandTimeout As Integer = 0) As Integer
        Dim blnLocalTransaction As Boolean = False
        If transaction Is Nothing Then
            transaction = m_objConnection.BeginTransaction()
            blnLocalTransaction = True
        End If

        Try
            Dim command As OleDbCommand = New OleDbCommand(sqlCommand, m_objConnection, transaction)
            If params IsNot Nothing Then
                For Each param As OleDbParameter In params
                    command.Parameters.Add(param)
                Next
            End If
            If commandTimeout > 0 Then command.CommandTimeout = commandTimeout
            Dim result As Integer = command.ExecuteNonQuery()
            command.Dispose()
            command = Nothing

            If blnLocalTransaction Then transaction.Commit()

            Return result
        Catch ex As Exception
            If blnLocalTransaction Then transaction.Rollback()
            Throw
        End Try
    End Function


    ' Provides a reference to a new transaction
    Public Function GetTransaction() As OleDbTransaction
        Return m_objConnection.BeginTransaction()
    End Function


#Region " IDisposable Support "

    ' To detect redundant calls
    Private _disposed As Boolean = False

    ' This is where release of resources should be implemented
    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        ' IMPORTANT - Do not change this code!
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ' This function is used to release internal resources
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not Me._disposed Then
            If disposing Then
                ' Free other state (managed objects).
                If m_objConnection.State = ConnectionState.Open Then
                    m_objConnection.Close()
                    m_objConnection = Nothing
                End If
            End If

            ' Free your own state (unmanaged objects).
            ' Set large fields to null.
        End If

        Me._disposed = True
    End Sub

#End Region

End Class
