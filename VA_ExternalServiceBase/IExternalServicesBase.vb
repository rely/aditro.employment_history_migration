﻿Public Interface IExternalServicesBase

    Function ExecuteService() As ResultLevel

    Property CustomerId() As String
    Sub SetInputFile(ByVal p_strFileName As String)
    Function GetOutputFile() As String

    Function GetExceptions() As String
    Function GetWarnings() As String
    Function GetInformation() As String

End Interface
